package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type HandlerFunc func(w http.ResponseWriter, r *http.Request) error

type APIError struct {
	Message string `json:"message"`
}

func (e *APIError) Error() string {
	return e.Message
}

type AlertRequest struct {
	StateCode string `json:"stateCode"`
}

type AlertResponse struct {
	Alerts []string `json:"alerts"`
}

func HandleErrors(handler HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := handler(w, r)
		if err != nil {
			e := APIError{Message: err.Error()}

			// with more time, handle error codes based on error
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(e)
		}
	}
}

func HandleAlerts(w http.ResponseWriter, r *http.Request) error {
	var area string
	var apiError APIError

	switch r.Method {
	case "GET":
		area = "NY"
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			apiError.Message = err.Error()
			return &apiError
		}

		var alertRequest AlertRequest
		err = json.Unmarshal(body, &alertRequest)
		if err != nil {
			apiError.Message = err.Error()
			return &apiError
		}

		area = alertRequest.StateCode
	default:
		apiError.Message = "allowed methods are: GET, POST"
		return &apiError
	}

	client := &http.Client{}
	weatherResponse, err := client.Get("http://api.weather.gov/alerts?area=" + area)
	if err != nil {
		apiError.Message = "error fetching alerts from: " + area
		return &apiError
	}

	alertResponse, err := weatherResponseToAlertResponse(weatherResponse)
	if err != nil {
		apiError.Message = err.Error()
		return &apiError
	}

	if apiError.Message == "" {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(alertResponse)
	}

	return nil
}

func weatherResponseToAlertResponse(r *http.Response) (AlertResponse, error) {
	var alertResponse AlertResponse
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return alertResponse, &APIError{Message: "could not read response for area: " + r.Request.URL.Query().Get("area")}
	}

	var data map[string]interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		return alertResponse, &APIError{Message: "could not parse response for area: " + r.Request.URL.Query().Get("area")}
	}

	if data["features"] == nil {
		return alertResponse, &APIError{Message: "could not find features for area: " + r.Request.URL.Query().Get("area")}
	}

	// with more time, make a more generic JSON parser or use 3rd party lib
	features := data["features"].([]interface{})
	alerts := make([]string, len(features))

	for i := 0; i < len(features); i++ {
		feature := features[i].(map[string]interface{})
		props := feature["properties"].(map[string]interface{})
		alerts[i] = props["headline"].(string)
	}

	alertResponse.Alerts = alerts
	return alertResponse, err
}
