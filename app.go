package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
)

func handleRequests(port string) {
	http.HandleFunc("/api/weather", HandleErrors(HandleAlerts))
	fmt.Printf("weather api listening @ http://localhost:%s\n", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func main() {
	port := flag.String("port", "8080", "local api port to listen on")
	flag.Parse()

	handleRequests(*port)
}
