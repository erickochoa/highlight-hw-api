# Highlight Weather API Homework

## Installation and Setup
1. Clone the repo: 
```
git clone https://erickochoa@bitbucket.org/erickochoa/highlight-hw-api.git
```
2. Navigate inside the project
```
cd highlight-hw-api
```
3. While inside the directory, run the exectuble:

```
Usage: ./weather [-port]

Options:
    -port: local api port (8080 by default) 
```

Or run to show help:
```
./weather -h
```

## Examples
This API supports the following methods:

GET: /api/weather

Example Response (shortened for readablity):
```
{
    "alerts": [
        "Special Weather Statement issued August 14 at 4:32PM EDT by NWS Upton NY",
        "Special Weather Statement issued August 14 at 4:01PM EDT by NWS Upton NY",
        "Special Weather Statement issued August 14 at 2:38PM EDT by NWS Upton NY",
        ...
    ]
}
```

POST: /api/weather

Example Request Payload:
```
{
    "stateCode": "CA"
}
```

Example Response (shortened for readbility):
```
{
    "alerts": [
        "The Fire Weather Watch has been replaced. Please see the latest information from NWS Medford OR on this developing situation.",
        "The Fire Weather Watch has been replaced. Please see the latest information from NWS Medford OR on this developing situation.",
        "Red Flag Warning issued August 16 at 1:45PM PDT until August 17 at 10:00PM PDT by NWS Medford OR",
        ...
    ]
}
```